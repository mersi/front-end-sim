#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <boost/program_options.hpp>

#include <TTree.h>
#include <TRandom3.h>
#include <TFile.h>
#include <TMath.h>
#include <TDatabasePDG.h>
#include <TVector3.h>
#include <Math/Point3D.h>
#include <Math/Point3Dfwd.h>

TRandom3 RV;
TDatabasePDG Database;

TDatabasePDG GDatabase;
namespace po = boost::program_options;
using CMS_GPoint = ROOT::Math::XYZPoint;

struct Event
{
	double Eta;
	CMS_GPoint GPosition;
	TVector3 GMomentum;
	TVector3 LDirection;
	double pT;
	double Energy;
	Int_t pdgid;
	double alpha;
	double beta;

	Event(const CMS_GPoint Global_Position, const TVector3 Global_Momentum, const TVector3 Local_Direction, Int_t _pdgid)
	{
		GPosition = Global_Position;
		GMomentum = Global_Momentum; // GeV
		LDirection = Local_Direction;
		TParticlePDG* Particle = GDatabase.GetParticle(_pdgid);
		double Mass = Particle->Mass(); // GeV
		pdgid = _pdgid;
		Energy = TMath::Sqrt(Global_Momentum.Mag2() + Mass * Mass) - Mass;		
		Eta = GPosition.Eta();
		pT = TMath::Sqrt(Global_Momentum.X() * Global_Momentum.X() + Global_Momentum.Y() * Global_Momentum.Y());
		alpha = TMath::ATan((LDirection.X() / LDirection.Z())) * TMath::RadToDeg();
		beta = TMath::ATan((LDirection.Y() / LDirection.Z())) * TMath::RadToDeg();
	}

	void Print()
	{
		std::cout << "GP: " << GPosition << " cm, " << "GM: (" << GMomentum(0) << ", " << GMomentum(1) << ", " << GMomentum(2) << ") GeV, pT: " << pT << " GeV, E: " << Energy << " GeV, PID: " << pdgid << "." << std::endl;
	}
};

int root_maker(std::string _CMSSW_SOURCE_NAME_, std::string _SAVE_NAME_, unsigned long int MAX_EVENTS)
{
	TFile* _file = TFile::Open(_CMSSW_SOURCE_NAME_.c_str());
	TTree* _ttree = (TTree*)_file->FindObjectAny("PixelSimHitNtuple");	
	
	TFile *_save = new TFile(_SAVE_NAME_.c_str(), "NEW", "output");
    TTree *_tree = new TTree("tree", "Main");

	for (auto i : *(_ttree->GetListOfBranches())) {std::cout << i->GetName() << std::endl;}

	_save->cd();
	unsigned long int Entries = _ttree->GetEntries();

	/** New TTree Branches && Connecting **/
	
	int EVENT;
	int sBX;
	int PDGID;
	float sltx, slty, sltz;
	float sgx, sgy, sgz, seta;
	float spt;
	float sltof;
	float sE;

	_tree->Branch("idEvent", &EVENT);
	_tree->Branch("pdg", &PDGID);
	//_tree->Branch("BX", &sBX);
	_tree->Branch("px", &sltx);
	_tree->Branch("py", &slty);
	_tree->Branch("pz", &sltz);
	_tree->Branch("ltof", &sltof);
	_tree->Branch("gx", &sgx);
	_tree->Branch("gy", &sgy);
	_tree->Branch("gz", &sgz);
	_tree->Branch("eta", &seta);
	_tree->Branch("pt", &spt);
	_tree->Branch("E", &sE);

	/** CMSSW Source TTree Branches && Refs **/

	Int_t BX[2];
	Int_t pdgid;
	Float_t ltx, lty, ltz;
	Float_t gx, gy, gz;
	Float_t px, py, pz;
	Float_t ltof;
	Int_t subid, layer, disk, ring, module, ladder, side;

	_ttree->SetBranchAddress("evt", &BX);
	_ttree->SetBranchAddress("pdgid", &pdgid);

	_ttree->SetBranchAddress("subid", &subid);
	_ttree->SetBranchAddress("side", &side);
	_ttree->SetBranchAddress("layer", &layer);
	_ttree->SetBranchAddress("ladder", &ladder);
	_ttree->SetBranchAddress("module", &module);
	_ttree->SetBranchAddress("disk", &disk);
	_ttree->SetBranchAddress("ring", &ring);

	_ttree->SetBranchAddress("ltx", &ltx);
	_ttree->SetBranchAddress("lty", &lty);
	_ttree->SetBranchAddress("ltz", &ltz);

	_ttree->SetBranchAddress("px", &px);
	_ttree->SetBranchAddress("py", &py);
	_ttree->SetBranchAddress("pz", &pz);

	_ttree->SetBranchAddress("gx", &gx);
	_ttree->SetBranchAddress("gy", &gy);
	_ttree->SetBranchAddress("gz", &gz);

	_ttree->SetBranchAddress("ltof", &ltof);

	/** ***** **/

	unsigned long int num = 0;
	for (unsigned long evt = 0; evt < Entries; ++evt)
	{
		_ttree->GetEntry(evt);
		if (!Database.GetParticle(pdgid)) {continue;}
		//bool criteria = (subid == 1 && layer == 2 && (ladder == 4) && module == 9);
		//subid == 2 and ring == 1 and disk == 1 and module == 1 and ladder == 4 and side == 1
		bool criteria = (subid == 2 && ring == 1 && disk == 5 && module == 1 && ladder == 4 && side == 1);
		if (criteria && num < MAX_EVENTS)
		{
			EVENT = num;
			num++;
			//sBX = BX[1];
			PDGID = pdgid;


			sltx = (bool)(ladder % 2) ? -ltx : ltx;
			slty = lty;
			sltz = ltz;


			sgx = RV.Gaus(0, 1);
			sgy = RV.Gaus(0, 1);
			sgz = (ltz > 0) ? -0.075 : +0.075;


			sltof = ltof;



			CMS_GPoint EVT_G_Position(gx, gy, gz);
			TVector3 EVT_G_Momentum(px, py, pz);
			TVector3 EVT_L_Direction(ltx, lty, ltz);
			printf("\033[1;32m(WRITING EVENT %lld)\033[0;37m\n", num);
			Event EVT(EVT_G_Position, EVT_G_Momentum, EVT_L_Direction, PDGID);
			
			
			sE = EVT.Energy * 1000;			
			seta = EVT.Eta;
			spt = EVT.pT;

			EVT.Print();

			_tree->Fill();
		}
		else if (num >= MAX_EVENTS) {break;}
	}
	_tree->Write();
	_save->Close();
	_file->Close();
	return 0;
}

int main(int ac, char *av[])
{
    std::string _CMSSW_PATH_;
    std::string _OUTPUT_NAME_;
    unsigned long int EVENTS = 1e5; // GeV

    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "produce help message")
    ("CMSSW_SOURCE", po::value<std::string>(&_CMSSW_PATH_)->required(), "relative path of CMSSW source root file")
    ("O", po::value<std::string>(&_OUTPUT_NAME_)->required(), "name of the output file")
    ("EVENTS", po::value<unsigned long int>(&EVENTS)->required(), "max events considered");

    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if (vm.count("help")) 
    {
       std::cout << desc << "\n";
       return 1;
    }

    root_maker(_CMSSW_PATH_, _OUTPUT_NAME_, EVENTS);
}
