import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.optimize import curve_fit
import matplotlib.ticker as mtick
from scipy.interpolate import make_interp_spline

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})
plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

FILE = np.genfromtxt("INB_LAY3_LAD4_M5.csv", delimiter = ", ")
#FILE = np.genfromtxt("ex.csv", delimiter = ", ")
fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (7.5,3.2))
ax.set_title(r"IB, Module 5, Layer 3, Ladder 4", fontsize = 15)

CUT = np.unique(FILE[:, 1])
NORM = np.linspace(start = 0, stop = 1, num = len(CUT), endpoint = True)
MAX_EFF = []
print(len(NORM), " ", len(CUT))

for i in range(0, len(CUT)):
    rgb = cm.inferno(NORM[i])
    X = -FILE[FILE[:, 1] == CUT[i], 0]
    Y = FILE[FILE[:, 1] == CUT[i], 2] / FILE[FILE[:, 1] == CUT[i], 3]
    ax.plot(X, Y, lw = 2.0, color = rgb, label = r"$p_{\text{T}} \geq " + "{:.1f}".format(CUT[i]) + "$ GeV")
    MAX_EFF.append(max(FILE[FILE[:, 1] == CUT[i], 2]))

#MAX_EFF = max(FILE[:, 1])
#DT_OPT = FILE[:,0][np.argmax(FILE[:, 1])]
#ax.axvline(DT_OPT, label = r"Optimal $\varepsilon_{\text{C}} = $" + " {:.2f}\% @ {:.1f} ns".format(MAX_EFF * 100, DT_OPT), color = 'red', zorder = 1)
#ax.scatter(DT_OPT, MAX_EFF)

ax.set_xlabel(r"Global DAQ Shift [ns]", fontsize = 15)
ax.set_ylabel(r"Cluster Efficiency $\varepsilon_{\text{C}}$", fontsize = 15)
ax.legend(fontsize = 11, loc = "upper right")
ax.grid()
ax.set_xlim([-10, 5])

Y_MIN = np.floor(max(FILE[FILE[:, 1] == 0, 2] / FILE[FILE[:, 1] == 0, 3]) * 100) / 100
Y_MAX = 1.001

ax.set_ylim([Y_MIN, Y_MAX])
ax.set_yticks(np.arange(start = Y_MIN, stop = Y_MAX, step = 0.0025))
ax.yaxis.set_major_formatter(mtick.PercentFormatter(decimals = 2, xmax = 1))
ax.axhline(1, ls = '--', color = 'black')
fig.savefig("EFF_MinBias_INB_L2_L4_M5.png", dpi = 500, bbox_inches='tight')

#def func(x, a):
#    return 1-(1-0.96) * np.exp(-a * x**0.9)

#fig1, ax1 = plt.subplots(nrows = 1, ncols = 1, figsize = (8,4))
#ax1.set_xlabel(r"pT Cut $(\geq)$ [GeV]", fontsize = 15)
#ax1.set_ylabel(r"Efficiency $\varepsilon$", fontsize = 15)
#ax1.scatter(x = CUT, y = MAX_EFF, marker = 'o', edgecolor = 'black', facecolor = 'white', ls = '-')
#popt, covt = curve_fit(func, CUT, MAX_EFF, p0 = [1])
#ax1.plot(np.linspace(start = min(CUT), stop = max(CUT), num = 1000), func(np.linspace(start = min(CUT), stop = max(CUT), num = 1000), popt[0]), ls = '-', color = 'black')
#ax1.grid()
#ax1.axhline(1, ls = '--', color = 'black')

plt.show(block = True)
