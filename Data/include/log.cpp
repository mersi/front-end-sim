#include <log.hpp>

LOGGER::LOGGER()
{
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    printf (M("*", w.ws_col).c_str());
    std::cout << "" << std::endl;
}
LOGGER::~LOGGER(){}
std::string LOGGER::M(const std::string a, unsigned long int b)
{
    std::string output = "";
    while (b--) {output += a;}
    return output;
}
std::string LOGGER::COLOR(const std::string _TEXT_, const std::string _FCOLOR_, const std::string _RCOLOR_ = _RETURN_)
{
    return (_FCOLOR_ + _TEXT_ + _RCOLOR_);
}
void LOGGER::SendFatal(const std::string& _MESSAGE_)
{
    std::cout << COLOR("(FATAL)", _RED_) + COLOR(_ARROW_, _YELLOW_) + _MESSAGE_ << std::endl;
}
void LOGGER::SendSuccess(const std::string& _MESSAGE_)
{
    std::cout << COLOR("(SUCCESS)", _GREEN_) + COLOR(_ARROW_, _YELLOW_) + _MESSAGE_ << std::endl;
}
void LOGGER::SendWarning(const std::string& _MESSAGE_)
{
    std::cout << COLOR("(WARNING)", _YELLOW_) + COLOR(_ARROW_, _YELLOW_) + _MESSAGE_ << std::endl;
}
void LOGGER::SendInfo(const std::string& _MESSAGE_)
{
    std::cout << COLOR("(INFO)", _BLUE_) + COLOR(_ARROW_, _YELLOW_) + _MESSAGE_ << std::endl;
}