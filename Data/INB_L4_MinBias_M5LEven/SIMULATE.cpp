#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <string>
#include <boost/program_options.hpp>

namespace POptions  = boost::program_options;

int main(int ac, char *av[])
{	
	std::string CMSSW_SOURCE_PATH;

	POptions::options_description desc("Allowed Executable Options");
	desc.add_options()
	("help", "Produces Help Message")
	("CMSSW_SOURCE_PATH", POptions::value<std::string>(&CMSSW_SOURCE_PATH)->required(), "Relative Path of the Main Configuration File");

	POptions::variables_map VAR_MAP;
	POptions::store(POptions::parse_command_line(ac, av, desc), VAR_MAP);
	POptions::notify(VAR_MAP);

	std::cout << CMSSW_SOURCE_PATH << std::endl; 
	
	TFile* _MAIN_ = TFile::Open(CMSSW_SOURCE_PATH.c_str());

	std::cout << _MAIN_ << std::endl;

	TTree* _TTREE_ = (TTree*)_MAIN_->FindObjectAny("tree");

	std::string COMMAND = "./.SIMULATE " + CMSSW_SOURCE_PATH + " " + std::to_string(_TTREE_->GetEntries());
	std::system(COMMAND.c_str());
	std::system("../../allpix-squared-g4gun/bin/allpix -c Main.conf");
	return 0;
}
