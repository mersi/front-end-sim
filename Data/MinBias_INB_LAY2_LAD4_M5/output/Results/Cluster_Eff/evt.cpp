#define SENSOR_THICKNESS 0.150
#define PITCH_X 0.025
#define PITCH_Y 0.100

#include <PixelHit.hpp>
#include <MCParticle.hpp>
#include <cluster.hpp>
#include <load.hpp>
#include <algorithm>
#include <log.hpp>

#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <iostream>
#include <TMath.h>
#include <string>
#include <vector>
#include <fstream>
#include <TRandom3.h>
#include <TDatabasePDG.h>
#include <boost/program_options.hpp>

namespace po = boost::program_options;
std::string DETECTOR_NAME = "Detector";
TRandom3 RV;
LOGGER _GLOBAL_LOG_;
TDatabasePDG GDatabase;
const std::string STATUS = "INFO";

double TIME_WALK(double _charge, double _threshold) 
{
    double TIME_WALK = (6.24573346e-04 * _threshold + 7.07441732) * exp(-(_charge - _threshold)/(0.0555666 * _threshold + 24.84357041) ) + (16.177236798564252 * _threshold + 2527.537067765581) / _charge + (-0.00038665573601304583 * _threshold);
    // + 14.324674481669573
    return TIME_WALK;
}

void viewer(std::string _PATH_, std::string _GEN_PATH_ , std::string _OUTPUT_NAME_, double _THRESHOLD_ = 1000)
{
    /** PREPARING THE OUTPUT FILE **/
    std::ofstream output;
    output.open(_OUTPUT_NAME_.c_str());
    /*******************************/
    
    /** SETTING UP THE ALLPIX OUTPUT FILE **/
    NodeFile Example(_PATH_, "LOG");
    std::vector<allpix::PixelHit*>& PixelHits = Example.GetPixelHits();
    std::vector<allpix::MCParticle*>& MCParticles = Example.GetMCParticles();
    unsigned long int Events = Example.GetEvents();
    /***************************************/

    /** GENERATOR ROOT FILE **/
    TFile* _main = TFile::Open(_GEN_PATH_.c_str());
    TTree* _tree = (TTree*)_main->FindObjectAny("tree");
    ::_GLOBAL_LOG_.SendInfo("Found Generator ROOT File with " + std::to_string(_tree->GetEntries()) + " Events.");
    /***************************************/

    /** SETTING UP BRANCHES && VARIABLES **/
    Float_t ltof;
    Float_t pt;
    Float_t E;
    _tree->SetBranchAddress("ltof", &ltof);
    _tree->SetBranchAddress("pt", &pt);
    _tree->SetBranchAddress("E", &E);
    /***************************************/

    for (double pt_lower_bound = 0; pt_lower_bound <= 1; pt_lower_bound += 0.1)
    {
        for (double dt = -10; dt < +10; dt += 0.1)
        {
            unsigned long int N_eff = 0;
            unsigned long int N_all = 0;

            for (unsigned long int _evt_ = 0; _evt_ < Events; _evt_++)
            {
                Example.SetEntry(_evt_); // Sets the PixelHits && MCParticles
                _tree->GetEntry(_evt_);
                if (STATUS == "DEBUG") std::cout << "EVENT #" << _evt_ << std::endl;

                /** APPLYING THRESHOLD **/
                std::vector<allpix::PixelHit*> MOD_PIXELHITS;
                for (auto& pixhit : PixelHits) {if (pixhit->getSignal() > _THRESHOLD_) MOD_PIXELHITS.push_back(pixhit);}
                /************************/

                Clusters CLUSTERS(MOD_PIXELHITS, MCParticles, 0);
                if (MOD_PIXELHITS.size() == 0 || pt < pt_lower_bound) {continue;}
                
                // if (MOD_PIXELHITS.size() == 0) {continue;}
                bool is_eff = 0;
                for (unsigned long int index = 0; index < CLUSTERS.GetClusterVector().size(); index++)
                {
                    auto C = CLUSTERS.GetClusterVector()[index];

                    if (STATUS == "DEBUG") std::cout << "  Cluster #" << index + 1 << " [Size: " << C.GetSize() << ", Height: " << C.GetHeight() << ", Width: " << C.GetWidth() << "]:" << std::endl;
                    for (auto& pixhit : C.GetPixelHitList())
                    {
                        double time = TIME_WALK(pixhit->getSignal(), _THRESHOLD_) + ltof + dt;// + RV.Rndm();
                        if (time >= 0 && time < 25 && is_eff == 0) is_eff = 1;
                        if (STATUS == "DEBUG") std::cout << "      > Event Efficiency Set to " << is_eff << std::endl;
                        bool pix_eff = (time >= 0 && time < 25) ? 1 : 0;
                        std::string BX = (pix_eff) ? "\033[1;32m" + std::to_string(time) + "\033[0;37m" : "\033[1;31m" + std::to_string(time) + "\033[0;37m";
                        if (STATUS == "DEBUG") std::cout << "      > LOC: (" << pixhit->getPixel().getLocalCenter().x() << ", " << pixhit->getPixel().getLocalCenter().y() << ") mm, SIGNAL: " << pixhit->getSignal() << "e, TIMIMG (ltof + TW): " << ltof << " + " << TIME_WALK(pixhit->getSignal(), _THRESHOLD_) << " = " << BX << " ns" << std::endl;
                        // output << _evt_ << ", " << index + 1 << ", " << pixhit->getPixel().getLocalCenter().x() << ", " << pixhit->getPixel().getLocalCenter().y() << ", " << pixhit->getSignal() << ", " << TIME_WALK(pixhit->getSignal(), _THRESHOLD_) + ltof << ", " << pt << std::endl;
                    }
                    bool One = (C.GetMCParticles().size() == 1) ? 1 : 0;
                    if (One)
                    {
                        auto MCP = C.GetMCParticles()[0];
                        auto Name = (GDatabase.GetParticle(MCP->getParticleID()) != nullptr) ? (GDatabase.GetParticle(MCP->getParticleID())->GetName()) : "Empty";
                        if (STATUS == "DEBUG") std::cout << "  by MCParticle @" << MCP << ": " << std::endl;
                        if (STATUS == "DEBUG") std::cout << "      > LOC: (" << MCP->getLocalStartPoint().x() << ", " << MCP->getLocalStartPoint().y() << ")" << ", PID: " << MCP->getParticleID() << " (" << Name << "), pT: " << pt << " GeV, E: " << E << " GeV" << std::endl;
                    }
                    else
                    {
                        if (STATUS == "DEBUG") std::cout << "  by MCParticles @" << (&C.GetMCParticles()[0]) << ": " << std::endl;
                        for (auto& MCP : C.GetMCParticles())
                        {
                            auto Name = (GDatabase.GetParticle(MCP->getParticleID()) != nullptr) ? (GDatabase.GetParticle(MCP->getParticleID())->GetName()) : "Empty";
                            if (STATUS == "DEBUG") std::cout << "      > LOC: (" << MCP->getLocalStartPoint().x() << ", " << MCP->getLocalStartPoint().y() << ")" << ", PID: " << MCP->getParticleID() << " (" << Name << "), E: " << " " << std::endl;
                        }
                    }
                }
                if (STATUS == "DEBUG") std::cout << "EVENT #" << _evt_ << ", EFFICIENT EVENT? " << is_eff << std::endl;
                N_eff += is_eff;
                N_all += 1;
            }
        _GLOBAL_LOG_.SendInfo("Efficiency of Run: " + (std::string)_GREEN_ + std::to_string((double)N_eff / N_all) + _RETURN_ + " @ " + std::to_string(dt) + " ns, @ " + std::to_string(pt_lower_bound) + " GeV/c Cut");
        // std::cout << "Sample Efficiency: " << (double)N_eff / N_all << " @ " << dt << " ns, @ " << pt_lower_bound << " GeV/c Cut" << std::endl;
        output << dt << ", " << pt_lower_bound << ", " << N_eff << ", " << N_all << std::endl;
        }
        if (STATUS == "DEBUG") std::cout << "*******************************************************************************************************************" << std::endl;
    }
    output.close();
}

int main(int ac, char *av[])
{
    std::string _PATH_;
    std::string _GEN_PATH_;
    std::string _OUTPUT_NAME_;
	double THRESHOLD = 1000;

	po::options_description desc("Allowed options");
	desc.add_options()
    ("help", "produce help message")
    ("PATH", po::value<std::string>(&_PATH_)->required(), "relative path of .root file")
    ("GEN_PATH", po::value<std::string>(&_GEN_PATH_)->required(), "relative path of generator .root file")
    ("O", po::value<std::string>(&_OUTPUT_NAME_)->required(), "relative path of generator .root file")
	("THR", po::value<double>(&THRESHOLD), "the value of the global chip threshold");

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
        std::cout << desc << "\n";
        return 1;
	}

    if (vm.count("path"))
    {
        std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Path: " << _PATH_ << std::endl;
    }

	_GLOBAL_LOG_.SendInfo("THRESHOLD == " + std::to_string((unsigned long int)THRESHOLD));

    viewer(_PATH_, _GEN_PATH_, _OUTPUT_NAME_, THRESHOLD);
    return 0;
}
