#define SENSOR_THICKNESS 0.150
#define PITCH_X 0.025
#define PITCH_Y 0.100

#include <PixelHit.hpp>
#include <MCParticle.hpp>
#include <cluster.hpp>
#include <load.hpp>
#include <log.hpp>

#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <iostream>
#include <TMath.h>
#include <string>
#include <vector>
#include <fstream>
#include <TRandom3.h>
#include <boost/program_options.hpp>

namespace po = boost::program_options;
std::string DETECTOR_NAME = "Detector";
TRandom3 RV;
LOGGER _GLOBAL_LOG_;

double TIME_WALK(double _charge, double _threshold) 
{
    double TIME_WALK = (6.24573346e-04 * _threshold + 7.07441732) * exp(-(_charge - _threshold)/(0.0555666 * _threshold + 24.84357041) ) + (16.177236798564252 * _threshold + 2527.537067765581) / _charge + (-0.00038665573601304583 * _threshold);
    // + 14.324674481669573
    return TIME_WALK;
}

void viewer(std::string _PATH_, std::string _GEN_PATH_ , std::string _OUTPUT_NAME_, std::string _MODE_, double _THRESHOLD_ = 1000, double _DELTA_DAQ_MIN_ = -50, double _DELTA_DAQ_MAX_ = 50, double _DELTA_DAQ_STEP_ = 1)
{
    std::ofstream output;
    output.open(_OUTPUT_NAME_.c_str());
    
    NodeFile Example(_PATH_, "LOG");
    std::vector<allpix::PixelHit*>& PixelHits = Example.GetPixelHits();
    std::vector<allpix::MCParticle*>& MCParticles = Example.GetMCParticles();

    TFile* _main = TFile::Open(_GEN_PATH_.c_str());
    TTree* _tree = (TTree*)_main->FindObjectAny("tree");
    std::cout << _tree->GetEntries() << std::endl;

    Float_t ltof;
    _tree->SetBranchAddress("ltof", &ltof);

    for (double DELTA_DAQ = _DELTA_DAQ_MIN_; DELTA_DAQ < _DELTA_DAQ_MAX_; DELTA_DAQ += _DELTA_DAQ_STEP_)
    {
        ::_GLOBAL_LOG_.SendInfo("Running Global DAQ Shift := " + std::to_string(DELTA_DAQ) + " ns");

        unsigned long int N_CBX_ltof = 0;
        unsigned long int N_CBX_tw = 0;
        unsigned long int N_CBX_clean = 0;
        unsigned long int N_CBX = 0;
        unsigned long int N_ALL = 0;

        for (unsigned long int _evt_ = 0; _evt_ < Example.GetEvents(); _evt_++)
        {
            Example.SetEntry(_evt_);
            Clusters __C__(PixelHits, MCParticles, 0);
            // if (__C__.GetNumber() != 1) {continue;}
            _tree->GetEntry(_evt_);
            
            unsigned long int counter_CBX = 0;
            unsigned long int counter_CBX_ltof = 0;
            unsigned long int counter_CBX_tw = 0;
            unsigned long int counter_CBX_clean = 0;

            for (auto& cluster : __C__.GetClusterVector())
            {
                for (auto& pixhit : cluster.GetPixelHitList())
                {
                    double signal = pixhit->getSignal();
                    if (signal < _THRESHOLD_) {continue;}

                    double time_ltof = ltof + DELTA_DAQ;
                    double time_tw = TIME_WALK(signal, _THRESHOLD_) + DELTA_DAQ;
                    double time_clean = DELTA_DAQ;
                    double time = ltof + TIME_WALK(signal, _THRESHOLD_) + RV.Rndm() + DELTA_DAQ;

                    if (time < 25 && time >= 0) {counter_CBX += 1;}
                    if (time_ltof < 25 && time >= 0) {counter_CBX_ltof += 1;}
                    if (time_tw < 25 && time_tw >= 0) {counter_CBX_tw += 1;}
                    if (time_clean < 25 && time_clean >= 0) {counter_CBX_clean += 1;}
                }                
            }

            if (counter_CBX > 0) {N_CBX++;}
            if (counter_CBX_ltof > 0) {N_CBX_ltof++;}
            if (counter_CBX_tw > 0) {N_CBX_tw++;}
            if (counter_CBX_clean > 0) {N_CBX_clean++;}
            N_ALL++;
        }

        double EFF_C = (double)N_CBX / (double)N_ALL;
        double EFF_ltof = (double)N_CBX_ltof / (double)N_ALL;
        double EFF_tw = (double)N_CBX_tw / (double)N_ALL;
        double EFF_clean = (double)N_CBX_clean / (double)N_ALL;
        std::cout << N_CBX_clean << ", " << N_ALL << std::endl;
        
	std::cout << N_CBX << " " << N_ALL << std::endl;
        ::_GLOBAL_LOG_.SendInfo("Writing Results to " + _OUTPUT_NAME_ + " >> " + std::to_string(DELTA_DAQ) + ", EFF_C = " + std::to_string(EFF_C) + ", EFF_ltof = " + std::to_string(EFF_ltof) + ", EFF_tw = " + std::to_string(EFF_tw) + ", EFF_clean = " + std::to_string(EFF_clean));
        output << DELTA_DAQ << ", " << EFF_C << ", " << EFF_ltof << ", " << EFF_tw << ", " << EFF_clean << std::endl;
        ::_GLOBAL_LOG_.SendInfo("Finished Global DAQ Shift := " + std::to_string(DELTA_DAQ) + " ns");
    }
    output.close();
}

int main(int ac, char *av[])
{
    std::string _PATH_;
    std::string _GEN_PATH_;
    std::string _OUTPUT_NAME_;
    std::string _MODE_;
	double THRESHOLD = 1000;
    double START = -50;
    double STOP = 50;
    double STEP = 1;

	po::options_description desc("Allowed options");
	desc.add_options()
    ("help", "produce help message")
    ("PATH", po::value<std::string>(&_PATH_)->required(), "relative path of .root file")
    ("GEN_PATH", po::value<std::string>(&_GEN_PATH_)->required(), "relative path of generator .root file")
    ("O", po::value<std::string>(&_OUTPUT_NAME_)->required(), "relative path of generator .root file")
    ("MODE", po::value<std::string>(&_MODE_)->required(), "relative path of generator .root file")
	("THR", po::value<double>(&THRESHOLD), "the value of the global chip threshold")
    ("START", po::value<double>(&START), "the value of the global chip threshold")
    ("STOP", po::value<double>(&STOP), "the value of the global chip threshold")
    ("STEP", po::value<double>(&STEP), "the value of the global chip threshold");

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
    		std::cout << desc << "\n";
    		return 1;
	}

    if (vm.count("path"))
    {
        std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Path: " << _PATH_ << std::endl;
    }

	_GLOBAL_LOG_.SendInfo("THRESHOLD == " + std::to_string((unsigned long int)THRESHOLD));

    viewer(_PATH_, _GEN_PATH_, _OUTPUT_NAME_, _MODE_, THRESHOLD, START, STOP, STEP);
    return 0;
}
