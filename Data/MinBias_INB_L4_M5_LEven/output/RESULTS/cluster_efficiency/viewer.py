import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})
plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

FILE = np.genfromtxt("test.csv", delimiter = ", ")
#FILE = np.genfromtxt("ex.csv", delimiter = ", ")
fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (8,4))
ax.set_title(r"$\simeq$ 50k \texttt{MinBias} Events @ IB, Module 5, Layer 4, Ladder Even", fontsize = 15)

ax.plot(FILE[:, 0], FILE[:, 1], color = 'blue', lw = 2.0, label = r"$\varepsilon_{\text{C}}$")
ax.plot(FILE[:, 0], FILE[:, 2], color = 'red', lw = 2.0, label = r"$\varepsilon_{\text{ltof}}$")
ax.plot(FILE[:, 0], FILE[:, 3], color = 'green', lw = 2.0, label = r"$\varepsilon_{\text{TW}}$")
ax.plot(FILE[:, 0], FILE[:, 4], color = 'black', lw = 2.0, label = r"$\varepsilon_{\text{clean}}$")
MAX_EFF = max(FILE[:, 1])
DT_OPT = FILE[:,0][np.argmax(FILE[:, 1])]
ax.axvline(DT_OPT, label = r"Optimal $\varepsilon_{\text{C}} = $" + " {:.2f}\% @ {:.1f} ns".format(MAX_EFF * 100, DT_OPT), color = 'red', zorder = 1)
ax.scatter(DT_OPT, MAX_EFF)
ax.set_xlabel(r"Global DAQ Shift [ns]", fontsize = 15)
ax.set_ylabel(r"Efficiency", fontsize = 15)
ax.legend(fontsize = 10, loc = "upper left")
#ax.set_xlim([-4, 15])
#ax.set_ylim([0.87, 1])
ax.grid()
fig.savefig("Eff.png", dpi = 500, bbox_inches='tight')
plt.show(block = True)
