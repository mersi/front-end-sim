import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})

FILE = np.genfromtxt("test.csv", delimiter = ", ")

fig, ax = plt.subplots(nrows = 1, ncols = 1)
ax.plot(FILE[:, 0], FILE[:, 1] / FILE[:, 2])
plt.show(block = True)
