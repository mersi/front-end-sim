import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.optimize import curve_fit
import matplotlib.ticker as mtick
from scipy.interpolate import make_interp_spline

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})
plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

FILE = np.genfromtxt("cluster_size.csv", delimiter = ", ")
fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (7.5,3.2))
ax.set_title(r"$\simeq$ 50k \texttt{MinBias} Events @ IB, Module 5, Layer 1, Ladder 4", fontsize = 15)

CUT = np.unique(FILE[:, 1])
NORM = np.linspace(start = 0, stop = 1, num = len(CUT), endpoint = True)
MAX_EFF = []
print(len(NORM), " ", len(CUT))

for i in range(0, len(CUT)):
    rgb = cm.inferno(NORM[i])
    X = -FILE[FILE[:, 1] == CUT[i], 0]
    Y = FILE[FILE[:, 1] == CUT[i], 4]
    ERROR = FILE[FILE[:, 1] == CUT[i], 5]

    Y_C = FILE[FILE[:, 1] == CUT[i], 2]
    ERROR_C = FILE[FILE[:, 1] == CUT[i], 3]

    ax.plot(X, Y_C, lw = 2.0, color = rgb, ls = '--', label = r"$p_{\text{T}}$ = " + "{:.2f} GeV".format(CUT[i]))
    #ax.plot(X, Y, lw = 1.0, color = rgb, ls = '-', label = r"$p_{\text{T}}$ = " + "{:.2f} GeV".format(CUT[i]))


    MAX_EFF.append(max(FILE[FILE[:, 1] == CUT[i], 2]))

ax.set_xlabel(r"Global DAQ Shift [ns]", fontsize = 15)
ax.set_ylabel(r"Cluster Size", fontsize = 15)
ax.legend(fontsize = 11, loc = "upper left")
ax.grid()
ax.set_xlim([-10,10])
fig.savefig("EFF_MinBias_INB_L4_LEven_CS.png", dpi = 500, bbox_inches='tight')
plt.show(block = True)
