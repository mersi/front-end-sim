import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.optimize import curve_fit
import matplotlib.ticker as mtick
from scipy.interpolate import make_interp_spline

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})
plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

FILE = np.genfromtxt("cluster_size.csv", delimiter = ", ")
fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (7.5,3.2))
ax.set_title(r"$\simeq$ 50k \texttt{MinBias} Events @ IB, Module 5, Layer 1, Ladder 4", fontsize = 15)

CUT = np.unique(FILE[:, 1])
DTs = np.unique(FILE[:, 0])
LOWERS = []
UPPERS = []
MEANS = []

for dt in range(0, len(DTs)):
    
    BIN_CONTENT = []
    BIN_CENTERS = []
    
    for i in range(2, 12):
        BIN_CONTENT.append(FILE[FILE[:, 0] == DTs[dt], :][0][ 2 * i + 1 ])
        BIN_CENTERS.append(FILE[FILE[:, 0] == DTs[dt], :][0][ 2 * i ] + 0.5)
    
    MAX = max(BIN_CONTENT)
    MAX_C = BIN_CENTERS[np.argmax(BIN_CONTENT)]

    BIN_CONTENT /= np.sum(BIN_CONTENT)

    MEAN = 0
    for i in range(0, len(BIN_CENTERS)):
        MEAN += BIN_CONTENT[i] * BIN_CENTERS[i]

    LOWER_ERROR = 0
    UPPER_ERROR = 0

    for i in range(0, len(BIN_CENTERS)):
        if (BIN_CENTERS[i] < MEAN):
            LOWER_ERROR += (BIN_CENTERS[i] - MEAN)**2 * BIN_CONTENT[i]
        else:
            UPPER_ERROR += (BIN_CENTERS[i] - MEAN)**2 * BIN_CONTENT[i]
    
    UPPER_ERROR = np.sqrt(UPPER_ERROR)
    LOWER_ERROR = np.sqrt(LOWER_ERROR)

    LOWERS.append(LOWER_ERROR)
    UPPERS.append(UPPER_ERROR)
    MEANS.append(MEAN)


ax.fill_between(-DTs, np.array(MEANS) - np.array(LOWERS), np.array(MEANS) + np.array(UPPERS), facecolor = 'orange', edgecolor = 'black', ls = '-', linewidth = 1.0, alpha = 0.5, label = r"$\pm \sigma^{\pm}$ (Wrong BX)")
ax.plot(-DTs, np.array(MEANS), color = 'black', ls = '--')
ax.set_xlabel(r"Global DAQ Shift [ns]", fontsize = 15)
ax.set_ylabel(r"Cluster Size", fontsize = 15)
ax.legend(fontsize = 11, loc = "upper left")
ax.grid()
ax.set_xlim([-10,10])
fig.savefig("EFF_MinBias_INB_L4_LEven_CS.png", dpi = 500, bbox_inches='tight')
plt.show(block = True)
