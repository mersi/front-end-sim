#include <TFile.h>
#include <iostream>
#include <TTree.h>

int checker()
{
	TFile* FILE = TFile::Open("INB_L4_M5.root");
	TTree* TTREE = (TTree*)(FILE->FindObjectAny("tree"));
	size_t EVENTS = TTREE->GetEntries();
	
	int pdgid;

	TTREE->SetBranchAddress("pdg", &pdgid);

	for (size_t i = 0; i < EVENTS; ++i)
	{
		TTREE->GetEntry(i);
		std::cout << pdgid << std::endl;

	}
	return 0;
}
