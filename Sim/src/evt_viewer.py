#!/usr/bin/env python3
import numpy as np
import argparse

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection


parser = argparse.ArgumentParser(description='Event Viewer Options')
parser.add_argument('--include_toolbar', action = 'store_true', help = 'if True, matplotlib toolbar is visible.')
parser.add_argument('--include_charges', action = 'store_true', help = 'if True, charge deposition on pixels is visible.')
parser.add_argument('--latex', action = 'store_true', help = 'if True, 3D view is compiled with Latex.')
args = parser.parse_args()

if not (args.include_toolbar): plt.rcParams['toolbar'] = 'None'
if (args.latex):
    plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})

def plot_cube(center, dimensions, ax):
    low_edge = np.array(center) - .5 * np.array(dimensions)
    x_push = [dimensions[0], 0, 0]
    y_push = [0, dimensions[1], 0]
    z_push = [0, 0, dimensions[2]]
    cube_definition = [low_edge, low_edge + np.array(x_push), low_edge + np.array(y_push), low_edge + np.array(z_push)]
    cube_definition_array = [
        np.array(list(item))
        for item in cube_definition
    ]
    points = []
    points += cube_definition_array
    vectors = [
        cube_definition_array[1] - cube_definition_array[0],
        cube_definition_array[2] - cube_definition_array[0],
        cube_definition_array[3] - cube_definition_array[0]
    ]

    points += [cube_definition_array[0] + vectors[0] + vectors[1]]
    points += [cube_definition_array[0] + vectors[0] + vectors[2]]
    points += [cube_definition_array[0] + vectors[1] + vectors[2]]
    points += [cube_definition_array[0] + vectors[0] + vectors[1] + vectors[2]]

    points = np.array(points)

    edges = [
        [points[0], points[3], points[5], points[1]],
        [points[1], points[5], points[7], points[4]],
        [points[4], points[2], points[6], points[7]],
        [points[2], points[6], points[3], points[0]],
        [points[0], points[2], points[4], points[1]],
        [points[3], points[6], points[7], points[5]]
    ]

    faces = Poly3DCollection(edges, linewidths=1, edgecolors='k')
    faces.set_facecolor((1,0,0,0.1)) #(0,0,1,0.1)
    ax.add_collection3d(faces)
    # Plot the points themselves to force the scaling of the axes
    ax.scatter(points[:,0], points[:,1], points[:,2], s=0)

PIXHITS = np.genfromtxt("event.csv", delimiter = ",")
MCPS = np.genfromtxt("mcps.csv", delimiter = ", ")

if (PIXHITS.shape == (5,)): PIXHITS = np.array([PIXHITS])
if (MCPS.shape == (7,)): MCPS = np.array([MCPS])

CLUSTERS = np.unique(PIXHITS[:,0])

fig = plt.figure("Event Viewer")
ax = fig.add_subplot(111, projection='3d')
ax.set_aspect('auto')

ax.set_ylim3d(bottom = -0.150, top = +0.150) 
ax.set_zlim3d(bottom = min(MCPS[:,2]) - 1 * 0.100, top = max(MCPS[:,5]) + 1 * 0.100) 
ax.set_xlim3d(left = min(MCPS[:,1]) - 1 * 0.025, right = max(MCPS[:,4]) + 1 * 0.025) 

for i in CLUSTERS:
    X = PIXHITS[PIXHITS[:,0] == i, 1]
    Y = PIXHITS[PIXHITS[:,0] == i, 2]
    Z = PIXHITS[PIXHITS[:,0] == i, 3]

    MCP_XS = MCPS[MCPS[:,0] == i, 1]
    MCP_YS = MCPS[MCPS[:,0] == i, 2]
    MCP_ZS = MCPS[MCPS[:,0] == i, 3]
    MCP_XE = MCPS[MCPS[:,0] == i, 4]
    MCP_YE = MCPS[MCPS[:,0] == i, 5]
    MCP_ZE = MCPS[MCPS[:,0] == i, 6]
    
    PIXHIT_VECTOR = []

    for hit in range(0, len(X)):
        PIXHIT_VECTOR.append([X[hit], Z[hit], Y[hit]])
    CHARGE = PIXHITS[PIXHITS[:,0] == i, 4]
    for i in PIXHIT_VECTOR:
        plot_cube(i, [0.025, 0.150, 0.100],ax)

    for mcp in range(0, len(MCP_XS)):
        XS = MCP_XS[mcp] - (MCP_XE[mcp] - MCP_XS[mcp]) * 2.0
        XE = MCP_XS[mcp] + (MCP_XE[mcp] - MCP_XS[mcp]) * 2.0
        YS = MCP_ZS[mcp] - (MCP_ZE[mcp] - MCP_ZS[mcp]) * 2.0
        YE = MCP_ZS[mcp] + (MCP_ZE[mcp] - MCP_ZS[mcp]) * 2.0
        ZS = MCP_YS[mcp] - (MCP_YE[mcp] - MCP_YS[mcp]) * 2.0
        ZE = MCP_YS[mcp] + (MCP_YE[mcp] - MCP_YS[mcp]) * 2.0
        ax.plot([XS, XE], [YS, YE], [ZS, ZE], color = 'green', lw = 1.5)
        ax.scatter(MCP_XS[mcp], MCP_ZS[mcp], MCP_YS[mcp], marker = 'o', color = 'blue', zorder = 20)
        ax.scatter(MCP_XE[mcp], MCP_ZE[mcp], MCP_YE[mcp], marker = 'o', color = 'blue', zorder = 20)
    for pixel_index in range(0, len(X)):
        if (args.include_charges and args.latex):
            ax.text(x = X[pixel_index], y = Z[pixel_index], z = Y[pixel_index], s = r"$\textbf{" + r"{:.0f}".format(CHARGE[pixel_index]) + r"}$ e$^-$", ha = 'center', va = 'center')
        else:
            ax.text(x = X[pixel_index], y = Z[pixel_index], z = Y[pixel_index], s = r"{:.0f}".format(CHARGE[pixel_index]) + r"e$^-$", ha = 'center', va = 'center')

ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.spines['left'].set_visible(False)
# ax.get_xaxis().set_ticks([])
# ax.get_yaxis().set_ticks([])
# ax.get_zaxis().set_ticks([])
ax.set_axis_off()
plt.show(block = True)
