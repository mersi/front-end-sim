#define SENSOR_THICKNESS 0.150
#define PITCH_X 0.025
#define PITCH_Y 0.100
#define SIZE_CUT 4

#include <PixelHit.hpp>
#include <MCParticle.hpp>
#include <cluster.hpp>
#include <load.hpp>
#include <log.hpp>

#include <TFile.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TH1D.h>
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <iostream>
#include <TMath.h>
#include <string>
#include <vector>
#include <fstream>
#include <TRandom3.h>
#include <boost/program_options.hpp>

namespace po = boost::program_options;
std::string DETECTOR_NAME = "Detector";
TRandom3 RV;

double TIME_WALK(double _charge, double _threshold) 
{
    double TIME_WALK = (6.24573346e-04 * _threshold + 7.07441732) * exp(-(_charge - _threshold)/(0.0555666 * _threshold + 24.84357041) ) + (16.177236798564252 * _threshold + 2527.537067765581) / _charge + (-0.00038665573601304583 * _threshold + 14.324674481669573);
    return TIME_WALK;
}


void viewer(std::string _PATH_, const double THRESHOLD, const double SIZE_CUT_X, const double SIZE_CUT_Y,
                                const double EFF_LOW_CHARGE_CUT_X, const double EFF_HIGH_CHARGE_CUT_X, const double EFF_LOW_CHARGE_CUT_Y, const double EFF_HIGH_CHARGE_CUT_Y)
{
        NodeFile Example(_PATH_, "LOG");
        std::vector<allpix::PixelHit*>& PixelHits = Example.GetPixelHits();
        std::vector<allpix::MCParticle*>& MCParticles = Example.GetMCParticles();

        TFile* _main = TFile::Open("../INB_L4_M5.root");
        TTree* _tree = (TTree*)_main->FindObjectAny("tree");

        Float_t ltx, lty, ltz, ltof;
        _tree->SetBranchAddress("px", &ltx);
        _tree->SetBranchAddress("py", &lty);
        _tree->SetBranchAddress("pz", &ltz);
        _tree->SetBranchAddress("ltof", &ltof);

	TH1D* Time = new TH1D("time", "Time Dispersion; Time [ns]; Frequency", 500, 0, 200);

        for (unsigned long int _evt_ = 0; _evt_ < Example.GetEvents(); _evt_++)
        {
            _tree->GetEntry(_evt_); 
            Example.SetEntry(_evt_);

            for (auto& pixhit : PixelHits)
            {
                double signal = pixhit->getSignal();
		if (signal < 1000) {continue;}
                double time = 0;
                time = ltof + TIME_WALK(signal, 1000) + RV.Gaus(0, 1);
		Time->Fill(time);
            }
        }
	TCanvas* c1 = new TCanvas("c1");
	c1->cd(); 
	Time->Draw();
	c1->Print("time_walk.png");
}

int main(int ac, char *av[])
{
    LOGGER _GLOBAL_LOG_;
    	std::string _PATH_;
	double THRESHOLD = 1000;
	double SIZE_CUT_X = 4;
	double SIZE_CUT_Y = 4;
	double EFF_LOW_CHARGE_CUT_X = 3;
	double EFF_HIGH_CHARGE_CUT_X = 0;
	double EFF_LOW_CHARGE_CUT_Y = 3;
	double EFF_HIGH_CHARGE_CUT_Y = 0;

	po::options_description desc("Allowed options");
	desc.add_options()
    ("help", "produce help message")
    ("path", po::value<std::string>(&_PATH_)->required(), "relative path of .root file")
	("THR", po::value<double>(&THRESHOLD), "the value of the global chip threshold")
	("SIZE_CUT_X", po::value<double>(&SIZE_CUT_X), "the value of the size cut on x")
	("SIZE_CUT_Y", po::value<double>(&SIZE_CUT_Y), "the value of the size cut on y")
	("EFF_LOW_CHARGE_CUT_X", po::value<double>(&EFF_LOW_CHARGE_CUT_X), "the value of the size cut on x")
	("EFF_HIGH_CHARGE_CUT_X", po::value<double>(&EFF_HIGH_CHARGE_CUT_X), "the value of the size cut on x")
	("EFF_LOW_CHARGE_CUT_Y", po::value<double>(&EFF_LOW_CHARGE_CUT_Y), "the value of the size cut on y")
	("EFF_HIGH_CHARGE_CUT_Y", po::value<double>(&EFF_HIGH_CHARGE_CUT_Y), "the value of the size cut on y");

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
    		std::cout << desc << "\n";
    		return 1;
	}

    if (vm.count("path"))
    {
        std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Path: " << _PATH_ << std::endl;
    }

	_GLOBAL_LOG_.SendInfo("THRESHOLD == " + std::to_string((unsigned long int)THRESHOLD));
	_GLOBAL_LOG_.SendInfo("SIZE CUT ON X == " + std::to_string((unsigned long int)SIZE_CUT_X));
	_GLOBAL_LOG_.SendInfo("SIZE CUT ON Y == " + std::to_string((unsigned long int)SIZE_CUT_Y));
	_GLOBAL_LOG_.SendInfo("EFFECTIVE WIDTH LOW CUT == " + std::to_string((unsigned long int)EFF_LOW_CHARGE_CUT_X));
	_GLOBAL_LOG_.SendInfo("EFFECTIVE WIDTH HIGH CUT == " + std::to_string((unsigned long int)EFF_HIGH_CHARGE_CUT_X));
	_GLOBAL_LOG_.SendInfo("EFFECTIVE HEIGHT LOW CUT == " + std::to_string((unsigned long int)EFF_LOW_CHARGE_CUT_Y));
	_GLOBAL_LOG_.SendInfo("EFFECTIVE HEIGHT HIGH CUT  == " + std::to_string((unsigned long int)EFF_HIGH_CHARGE_CUT_Y));


    viewer(_PATH_, THRESHOLD, SIZE_CUT_X, SIZE_CUT_Y, EFF_LOW_CHARGE_CUT_X, EFF_HIGH_CHARGE_CUT_X, EFF_LOW_CHARGE_CUT_Y, EFF_HIGH_CHARGE_CUT_Y);
    return 0;
}
