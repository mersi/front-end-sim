#define PITCH_X 0.025
#define PITCH_Y 0.100

/** AllpixSquared Object Header Files **/
#include <PixelHit.hpp>
#include <MCParticle.hpp>
#include <cluster.hpp>
#include <load.hpp>
#include <boost/program_options.hpp>
#include <log.hpp>
#include <iomanip>

/** ROOT Objects **/
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <iostream>
#include <TMath.h>
#include <string>
#include <vector>
#include <fstream>

namespace po = boost::program_options;
std::string DETECTOR_NAME = "Detector";

std::vector<unsigned long int> ReturnMinima(std::vector<double>& _INPUT_VEC_)
{
        const std::vector<double>::iterator START = _INPUT_VEC_.begin();
        const std::vector<double>::iterator END   = _INPUT_VEC_.end();
        unsigned long int min = std::min_element(START, END) - START;
        std::vector<unsigned long int> Min_Addresses {min};
        for (unsigned long int index = 0; index < _INPUT_VEC_.size(); ++index) if (_INPUT_VEC_[index] == _INPUT_VEC_[min] && index != min) {Min_Addresses.push_back(index);}
        return Min_Addresses;
}
std::vector<unsigned long int> ReturnMaxima(std::vector<double>& _INPUT_VEC_)
{
        const std::vector<double>::iterator START = _INPUT_VEC_.begin();
        const std::vector<double>::iterator END   = _INPUT_VEC_.end();
        unsigned long int max = std::max_element(START, END) - START;
        std::vector<unsigned long int> Max_Addresses {max};
        for (unsigned long int index = 0; index < _INPUT_VEC_.size(); ++index) if (_INPUT_VEC_[index] == _INPUT_VEC_[max] && index != max) {Max_Addresses.push_back(index);}
        return Max_Addresses;
}
std::vector<std::pair<double, double>> GetReconstructedPositions(Clusters& _EVT_CLUSTER_, const double& _alpha_, const double& _beta_, const double THRESHOLD, const double SIZE_CUT_X, const double SIZE_CUT_Y, 
				const double EFF_LOW_CHARGE_CUT_X, const double EFF_HIGH_CHARGE_CUT_X, const double EFF_LOW_CHARGE_CUT_Y, const double EFF_HIGH_CHARGE_CUT_Y)
{
    std::vector<std::pair<double, double>> output;
    for (auto& i : _EVT_CLUSTER_.GetClusterVector())
    {
        auto PixHits = i.GetPixelHitList();
        auto MCPs = i.GetMCParticles();

        double XR;
        double YR;
        {
            std::vector<double> X;
            std::vector<double> Y;
            std::vector<double> Charge;
            for (auto& pixhit : PixHits)
            {
		double Signal = pixhit->getSignal();
		if (Signal > THRESHOLD)
		{
                	X.push_back(pixhit->getPixel().getLocalCenter().x());
                	Y.push_back(pixhit->getPixel().getLocalCenter().y());
                	Charge.push_back(pixhit->getSignal());
		}
            }

	    if (!Charge.size()) {continue;}

            std::vector<unsigned long int> min_indices_x = ::ReturnMinima(X);
            std::vector<unsigned long int> min_indices_y = ::ReturnMinima(Y);
            std::vector<unsigned long int> max_indices_x = ::ReturnMaxima(X);
            std::vector<unsigned long int> max_indices_y = ::ReturnMaxima(Y);

            double XG = (*std::min_element(X.begin(), X.end()) + *std::max_element(X.begin(), X.end())) / 2.;
            double YG = (*std::min_element(Y.begin(), Y.end()) + *std::max_element(Y.begin(), Y.end())) / 2.;

            double charge_row_min = 0;
            double charge_row_max = 0;
            double charge_col_min = 0;
            double charge_col_max = 0;

            unsigned long int size_x = i.GetWidth();
            unsigned long int size_y = i.GetHeight();
            
            for (auto& index : min_indices_x) charge_row_min += Charge[index];
            for (auto& index : max_indices_x) charge_row_max += Charge[index];
            for (auto& index : min_indices_y) charge_col_min += Charge[index];
            for (auto& index : max_indices_y) charge_col_max += Charge[index];

            double multip_term_x = ((charge_row_max - charge_row_min) / (2 * (charge_row_min + charge_row_max)));
            double W_Inner_x = (size_x > 2) ? (size_x - 2) * PITCH_X : 0;
            double default_path_x = TMath::Abs(0.150 * TMath::Tan(TMath::DegToRad() * _alpha_));
	    double w_eff_x = std::abs(std::abs(default_path_x) - W_Inner_x);
            double abs_value_x = ((size_x >= SIZE_CUT_X) || (w_eff_x / PITCH_X < EFF_LOW_CHARGE_CUT_X | w_eff_x / PITCH_X > EFF_HIGH_CHARGE_CUT_X)) ? PITCH_X : w_eff_x;

            double multip_term_y = ((charge_col_max - charge_col_min) / (2 * (charge_col_min + charge_col_max)));
            double W_Inner_y = (size_y > 2) ? (size_y - 2) * PITCH_Y : 0;
            double default_path_y = TMath::Abs(0.150 * TMath::Tan(TMath::DegToRad() * _beta_));
            double w_eff_y = std::abs(std::abs(default_path_y) - W_Inner_y);
            double abs_value_y = ((size_y >= SIZE_CUT_Y) || (w_eff_y / PITCH_Y < EFF_LOW_CHARGE_CUT_Y | w_eff_y / PITCH_Y > EFF_HIGH_CHARGE_CUT_Y)) ? PITCH_Y : w_eff_y;

            XR = XG + multip_term_x * abs_value_x;
            YR = YG + multip_term_y * abs_value_y;
        }
        for (auto& mcp : MCPs)
        {
            std::pair<double, double> Rec_Pos = std::make_pair<double, double>(XR - mcp->getLocalStartPoint().x(), YR - mcp->getLocalStartPoint().y());
            output.push_back(Rec_Pos);
        }
    }
    return output;
}
const std::pair<double, double> Filename2Angles(const std::string& _FILENAME_)
{
    std::size_t a_char = _FILENAME_.find('a');
    std::size_t b_char = _FILENAME_.find('b');
    std::string alpha = _FILENAME_.substr(a_char + 1, b_char - 1);
    std::string beta = _FILENAME_.substr(b_char + 1);
    const std::pair<double, double> output = std::make_pair<double, double>(std::stod(alpha), std::stod(beta));
    return output;
}

void viewer(std::string _PATH_, const double THRESHOLD, const double SIZE_CUT_X, const double SIZE_CUT_Y,
                                const double EFF_LOW_CHARGE_CUT_X, const double EFF_HIGH_CHARGE_CUT_X, const double EFF_LOW_CHARGE_CUT_Y, const double EFF_HIGH_CHARGE_CUT_Y)
{
	// TH1D::AddDirectory(kFALSE); 
    TSystemDirectory* Curr_Dir = new TSystemDirectory("curr", _PATH_.c_str());
	auto ListofFiles = Curr_Dir->GetListOfFiles();
    std::ofstream output;
    output.open("output.csv");
	for (auto const& files : *ListofFiles)
	{
		std::string folder_name(files->GetName());
		if (folder_name[0] != '.')
		{
			auto Angles = Filename2Angles(folder_name);

            std::vector<double> HEIGHT;
            std::vector<double> WIDTH;
	    std::vector<double> CLUSTER_NUM;

            NodeFile Example(folder_name, _PATH_, "LOG");
            std::vector<allpix::PixelHit*>& PixelHits = Example.GetPixelHits();
            std::vector<allpix::MCParticle*>& MCParticles = Example.GetMCParticles();

            for (unsigned long int _evt_ = 0; _evt_ < Example.GetEvents(); ++_evt_)
            {
                Example.SetEntry(_evt_);
                Clusters __C__(PixelHits, MCParticles, 0);
		CLUSTER_NUM.push_back(__C__.GetClusterVector().size());
		for (auto& cluster : __C__.GetClusterVector())
		{
			unsigned long int _HEIGHT = cluster.GetHeight();
			unsigned long int _WIDTH = cluster.GetWidth();
			HEIGHT.push_back(_HEIGHT);
			std::cout << _HEIGHT << std::endl;
			WIDTH.push_back(_WIDTH);
		}
            }
            output << Angles.first << ", " << Angles.second << ", " << TMath::Mean(HEIGHT.begin(), HEIGHT.end()) << ", " << TMath::Mean(WIDTH.begin(), WIDTH.end()) << ", " << TMath::Mean(CLUSTER_NUM.begin(), CLUSTER_NUM.end()) << std::endl;
        }
    }
    output.close();
}

int main(int ac, char *av[])
{
	LOGGER _GLOBAL_LOG_;
    	std::string _PATH_;
	double THRESHOLD = 1000;
	double SIZE_CUT_X = 4;
	double SIZE_CUT_Y = 4;
	double EFF_LOW_CHARGE_CUT_X = 3;
	double EFF_HIGH_CHARGE_CUT_X = 0;
	double EFF_LOW_CHARGE_CUT_Y = 3;
	double EFF_HIGH_CHARGE_CUT_Y = 0;

	po::options_description desc("Allowed options");
	desc.add_options()
    ("help", "produce help message")
    ("path", po::value<std::string>(&_PATH_)->required(), "relative path of .root file")
	("THR", po::value<double>(&THRESHOLD), "the value of the global chip threshold")
	("SIZE_CUT_X", po::value<double>(&SIZE_CUT_X), "the value of the size cut on x")
	("SIZE_CUT_Y", po::value<double>(&SIZE_CUT_Y), "the value of the size cut on y")
	("EFF_LOW_CHARGE_CUT_X", po::value<double>(&EFF_LOW_CHARGE_CUT_X), "the value of the size cut on x")
	("EFF_HIGH_CHARGE_CUT_X", po::value<double>(&EFF_HIGH_CHARGE_CUT_X), "the value of the size cut on x")
	("EFF_LOW_CHARGE_CUT_Y", po::value<double>(&EFF_LOW_CHARGE_CUT_Y), "the value of the size cut on y")
	("EFF_HIGH_CHARGE_CUT_Y", po::value<double>(&EFF_HIGH_CHARGE_CUT_Y), "the value of the size cut on y");

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
    		std::cout << desc << "\n";
    		return 1;
	}

    if (vm.count("path"))
    {
        std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Path: " << _PATH_ << std::endl;
    }

	_GLOBAL_LOG_.SendInfo("THRESHOLD == " + std::to_string((unsigned long int)THRESHOLD));
	_GLOBAL_LOG_.SendInfo("SIZE CUT ON X == " + std::to_string((unsigned long int)SIZE_CUT_X));
	_GLOBAL_LOG_.SendInfo("SIZE CUT ON Y == " + std::to_string((unsigned long int)SIZE_CUT_Y));
	_GLOBAL_LOG_.SendInfo("EFFECTIVE WIDTH LOW CUT == " + std::to_string((unsigned long int)EFF_LOW_CHARGE_CUT_X));
	_GLOBAL_LOG_.SendInfo("EFFECTIVE WIDTH HIGH CUT == " + std::to_string((unsigned long int)EFF_HIGH_CHARGE_CUT_X));
	_GLOBAL_LOG_.SendInfo("EFFECTIVE HEIGHT LOW CUT == " + std::to_string((unsigned long int)EFF_LOW_CHARGE_CUT_Y));
	_GLOBAL_LOG_.SendInfo("EFFECTIVE HEIGHT HIGH CUT  == " + std::to_string((unsigned long int)EFF_HIGH_CHARGE_CUT_Y));


    viewer(_PATH_, THRESHOLD, SIZE_CUT_X, SIZE_CUT_Y, EFF_LOW_CHARGE_CUT_X, EFF_HIGH_CHARGE_CUT_X, EFF_LOW_CHARGE_CUT_Y, EFF_HIGH_CHARGE_CUT_Y);
    return 0;
}

