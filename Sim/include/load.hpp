#pragma once
#define IDENT(x) x
#define XSTR(x) #x
#define STR(x) XSTR(x)
#define PATH(_path, _header) STR(IDENT(_path)IDENT(_header).hpp)

#define _OBJECTS_PATH_ /cvmfs/clicdp.cern.ch/software/allpix-squared/2.0.3/x86_64-centos8-gcc11-opt/include/objects/
#define PIXELHIT PixelHit
#define PIXELCHARGE PixelCharge
#define MCPARTICLE MCParticle
#define MCTRACK MCTrack

#include PATH(_OBJECTS_PATH_, PIXELHIT)
#include PATH(_OBJECTS_PATH_, MCPARTICLE)

#include <vector>
#include <string>
#include <map>
#include <TTree.h>

class NodeFile
{
    public:
        NodeFile(std::string _PATH_, std::string _DEBUG_ = "WARNING");
        ~NodeFile();
        std::map<const std::string, TTree*> GetMap();
        std::vector<allpix::PixelHit*>& GetPixelHits();
        std::vector<allpix::MCParticle*>& GetMCParticles();
        unsigned long int GetEvents();
        void SetEntry(unsigned long int);
    private:
        unsigned long int M_ENTRIES;
        TFile* MAIN_FILE;
        std::map<const std::string, TTree*> TREE_MAP;
        TTree* PIXEL_HIT_TREE;
        TBranch* PIXEL_HIT_DBRANCH;
        std::vector<allpix::PixelHit*> PixelHits;
        TTree* MCPARTICLE_TREE;
        TBranch* MCPARTICLE_DBRANCH;
        std::vector<allpix::MCParticle*> MCParticles;
        TTree* MCTRACK_TREE;        
};
