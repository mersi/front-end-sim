#include <string>
#include <map>

namespace SECTIONS
{
    struct SIMULATE
    {
        std::string ALLPIX_PATH;
        std::string SOURCE_PATH;
        std::string LOCATIONS;
        std::string LOG_LEVEL = "WARNING";
        std::string TTREE_NAME = "PixelSimHitNtuple";
        std::string FILE_NAME = "./data.root";
        unsigned long int EVENTS = 20000;
        
        std::map<std::string, std::string*> REF = {
            {"allpix_path", &ALLPIX_PATH},
            {"log_level", &LOG_LEVEL},
            {"source_path", &SOURCE_PATH},
            {"ttree_name", &TTREE_NAME},
            {"locations", &LOCATIONS},
            {"file_name", &FILE_NAME}
        };
    };
}