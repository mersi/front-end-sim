#include <iostream>
#include <TFile.h>
#include <TTree.h>

#include <TBranch.h>
#include <TRandom3.h>
#include <TDatabasePDG.h>

TRandom3 RV;
TDatabasePDG Database;

int root_maker()
{
	TFile* _file = TFile::Open("./CMSSW.root");
	TTree* _ttree = (TTree*)_file->FindObjectAny("PixelSimHitNtuple");	
	
	TFile *_save = new TFile("INB_L4_MinBias_M5LEven.root", "NEW", "output");
        TTree *_tree = new TTree("tree", "Main");

	for (auto i : *(_ttree->GetListOfBranches())) {std::cout << i->GetName() << std::endl;}

	_save->cd();
	unsigned long int Entries = _ttree->GetEntries();
	
	int EVENT;
	int sBX;
	int PDGID;
	float sltx, slty, sltz;
	float sgx, sgy, sgz, seta;
	float spt;
	float sltof;
	float sE;

	_tree->Branch("idEvent", &EVENT);
	_tree->Branch("pdg", &PDGID);
	//_tree->Branch("BX", &sBX);
	_tree->Branch("px", &sltx);
	_tree->Branch("py", &slty);
	_tree->Branch("pz", &sltz);
	_tree->Branch("ltof", &sltof);
	_tree->Branch("gx", &sgx);
	_tree->Branch("gy", &sgy);
        _tree->Branch("gz", &sgz);
	_tree->Branch("eta", &seta);
	_tree->Branch("pt", &spt);
        _tree->Branch("E", &sE);

	Int_t BX[2];
        Int_t pdgid;
        Float_t ltx, lty, ltz;
        Float_t gx, gy, gz;
        Float_t ltof;
	Int_t subid, layer, disk, ring, module, ladder;

	_ttree->SetBranchAddress("evt", &BX);
	_ttree->SetBranchAddress("pdgid", &pdgid);
	_ttree->SetBranchAddress("subid", &subid);
	_ttree->SetBranchAddress("layer", &layer);
	_ttree->SetBranchAddress("ladder", &ladder);
	_ttree->SetBranchAddress("module", &module);
	_ttree->SetBranchAddress("ltx", &ltx);
	_ttree->SetBranchAddress("lty", &lty);
	_ttree->SetBranchAddress("ltz", &ltz);
	_ttree->SetBranchAddress("gx", &gx);
        _ttree->SetBranchAddress("gy", &gy);
        _ttree->SetBranchAddress("gz", &gz);
        _ttree->SetBranchAddress("ltof", &ltof);

	unsigned long int num = 0;
	unsigned long int MAX_NUM = 5e4;

	for (unsigned long evt = 0; evt < Entries; ++evt)
	{
		_ttree->GetEntry(evt);
		if (!Database.GetParticle(pdgid)) {std::cout << "Found " << pdgid << std::endl; continue;}
		bool criteria = (subid == 1 && layer == 4 && (module % 2 == 0));
		if (criteria && num <= MAX_NUM)
		{
			EVENT = num;
			num++;
			//sBX = BX[1];
			PDGID = pdgid;
			sltx = (bool)(ladder % 2) ? -ltx : ltx;
			slty = lty;
			sltz = ltz;
			
			sgx = RV.Gaus(0, 1);
			sgy = RV.Gaus(0, 1);
			sgz = (ltz > 0) ? -0.080 : +0.080;
			sltof = ltof;
			sE = 1000;
			ROOT::Math::XYZPoint CMSPoint(gx, gy, gz);
			seta = CMSPoint.Eta(); 
			_tree->Fill();
		}
	}
	_tree->Write();
	_save->Close();
	_file->Close();
	return 0;
}
