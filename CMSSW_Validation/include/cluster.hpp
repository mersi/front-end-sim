#pragma once
#define IDENT(x) x
#define XSTR(x) #x
#define STR(x) XSTR(x)
#define PATH(_path, _header) STR(IDENT(_path)IDENT(_header).hpp)

#define _OBJECTS_PATH_ /cvmfs/clicdp.cern.ch/software/allpix-squared/2.0.3/x86_64-centos8-gcc11-opt/include/objects/
#define PIXELHIT PixelHit
#define PIXELCHARGE PixelCharge
#define MCPARTICLE MCParticle
#define MCTRACK MCTrack

#include PATH(_OBJECTS_PATH_, PIXELHIT)
#include PATH(_OBJECTS_PATH_, MCPARTICLE)

#include <vector>

class Cluster
{
    public:
        Cluster();
        ~Cluster();
        std::vector<allpix::PixelHit*> GetPixelHitList();
        std::vector<const allpix::MCParticle*> GetMCParticles();
        unsigned long int GetWidth();
        unsigned long int GetHeight();
        bool IsNormal();
    private:
        std::vector<const allpix::MCParticle*> MCParticlesList;
        std::vector<allpix::PixelHit*> PixHitList;
        void Append(allpix::PixelHit*);
        unsigned int _size_x;
        unsigned int _size_y;
        unsigned int _size_;
        bool is_Normal;
        friend class Clusters;
};

class Clusters
{
    public:
        Clusters(std::vector<allpix::PixelHit*> PixelHits, std::vector<allpix::MCParticle*> MCParticles, bool DEBUG);
        ~Clusters();
        unsigned int GetNumber();
        std::vector<Cluster> GetClusterVector();
    private:
        bool _DEBUG_ = 0;
        std::vector<Cluster> ClusterVector;
        bool AreClose(allpix::PixelHit* PIXEL_1, allpix::PixelHit* PIXEL_2);
        void CompleteCluster(Cluster&, std::vector<allpix::PixelHit*>&, unsigned long int);
        unsigned int _size_;
};
