#ifndef CROCV1_H
#define CROCV1_H

/** Definition of Models for the RD53B-CMS Readout Chip **/
#include <cmath>
#include <fstream>
#include <vector>
#include <set>
#include <utility>
#include <string>
#include <algorithm>
#include <map>

/** Definition of Models for the RD53B-CMS Readout Chip **/
#include <TF1.h>
#include <TRandom3.h>

/** Definition of Models for the RD53B-CMS Readout Chip **/
#include <PixelHit.hpp>

/** Custom **/
#include <log.hpp>
#include <cluster.hpp>

class CROCv1
{
    public:
        typedef std::tuple<std::string, std::string, std::string> MODE;
        
        /** 
         * @param _threshold: The Global Threshold set in the chip, in electron units.
         * @param _krum: The Global Krummenacher (KRUM_CURR_LIN), in DAC units.
         */
        CROCv1(const double _threshold, const double _threshold_dispersion, const double _krum, const double _krum_dispersion, const double _clk_offset, 
                        const MODE _mode, const unsigned short int _isolated_hit_removal_max_ToT_, const double _electronic_noise_, const int _triggers, LOGGER& Messenger);

        /** Returns the point in time where the comparator edge rises.
         * @param _charge: The Input charge on the Front-End **/
        const double RisingEdge(const double _charge) const;

        /** Returns the point in time where the comparator edge falls.
         * @param _charge: The Input charge on the Front-End **/
        const double FallingEdge(const double _charge) const;

        /** Returns the time interval in which the comparator is up.
         * @param _charge: The Input charge on the Front-End **/
        const double TimeDuration(const double _charge) const;

        /** Returns ToT, given the mode of the RD53B_CMS Readout Chip.
         * @param _charge: The Input charge on the Analog Front-End.
         * @param _mode: The CROC Mode {{"Latched", "Synch"}, {"SingleEdged", "DoubleEdged"}, {"SingleSloped", "DoubleSloped"}}. **/

        const unsigned short int GetToT(double _charge);

        /** !Methods that use raw allpix Objects! **/

        /** Returns ToT, given the mode of the RD53B_CMS Readout Chip.
        * @param PixHit := The Pixel Hit Object, for which the ToT is to be extracted.
        * @param RefPixHits := The Reference Pixel Hits in the Event. */

        const unsigned short int GetToT(const allpix::PixelHit* PixHit, const std::vector<allpix::PixelHit*> RefPixHits) const;

        /** Returns ToT, given the mode of the RD53B_CMS Readout Chip.
        * @param PixHit := The Pixel Hit Object, for which the ToT is to be extracted.
        * @param RefPixHits := The Reference Pixel Hits in the Event. */

        const std::vector<allpix::PixelHit*> ApplySettings(const std::vector<allpix::PixelHit*> RefPixelHits) const;

        /** Returns A Reference ToT/charge Ladder @ given VTHRESHOLD_LIN, KRUM_CURR_LIN settings.
         * @param _threshold: The VTHRESHOLD_LIN setting, but in electron units.
         * @param _krum: The KRUM_CURR_LIN setting, but in DAC units.
         * @param _mode: The CROC Mode {{"Latched", "Synch"}, {"SingleEdged", "DoubleEdged"}, {"SingleSloped", "DoubleSloped"}} **/

        const std::map<unsigned short int, double> GetNominalReferenceLadder();

        /** Returns A Reference ToT/charge Ladder @ given VTHRESHOLD_LIN, KRUM_CURR_LIN settings.
         * @param _threshold: The VTHRESHOLD_LIN setting, but in electron units.
         * @param _krum: The KRUM_CURR_LIN setting, but in DAC units.
         * @param _mode: The CROC Mode {{"Latched", "Synch"}, {"SingleEdged", "DoubleEdged"}, {"SingleSloped", "DoubleSloped"}} **/

        const std::map<unsigned short int, std::tuple<double, double, double>> GetNominalReferenceLadderFull();

        /** Prints the Outcome of a single measurement a stores it in $file_name 
         * @param _charge: The charge injection. 
         * @param file_name: The name of the output file. **/

        /** !Print Methods! **/

        void PrintMeasurement(double _charge, std::string file_name);

        void PrintNominalReferenceLadder(std::string file_name);

        void PrintTornadoMap(std::string file_name);

        /** Nested Classes that Describe the Analog Front-End of the CROCv1
         * @param CROCv1_CMP := Description of the Comparator's behaviour.
         * @param CROCv1_CLK := Description of the Clock's behaviour. **/

        class CROCv1_CMP
        {
            public:
                CROCv1_CMP(const CROCv1* PARENT, const double _charge);
                const double GetRisingEdge() const;
                const double GetFallingEdge() const;
                const bool GetSignal(const double TimeRef) const;
            private:
                double RIS_EDGE;
                double FAL_EDGE;
        };

        class CROCv1_CLK
        {
            public:
                CROCv1_CLK(const CROCv1* PARENT);
                const bool GetSignal(const double TimeRef) const;
                const std::vector<double> CountRisingEdges(const std::pair<double, double>& INTERVAL) const;
                const std::vector<double> CountFallingEdges(const std::pair<double, double>& INTERVAL) const;
            private:
                double OFFSET;
                const double GetKthRisingEdge(const signed short int K) const;
                const double GetKthFallingEdge(const signed short int K) const;
        };

    private:

        /** Private Variables
         * @param THRESHOLD
         * @param THRESHOLD_DISPERSION
         * @param KRUM
         * @param KRUM_DISPERSION
         * @param CHIP_MODE
         * @param CLK_OFFSET
         * @param ISOLATED_HIT_MAX_TOT
         * @param ELECTRONIC_NOISE
         * @param TRIGGERS **/ 

        const double THRESHOLD;
        const double THRESHOLD_DISPERSION;
        const double KRUM;
        const double KRUM_DISPERSION;
        MODE CHIP_MODE;
        const double CLK_OFFSET;
        const unsigned short int ISOLATED_HIT_MAX_TOT;
        const double ELECTRONIC_NOISE;
        const int TRIGGERS;

        /** Returns ToT, given the mode of the CROCv1 Readout Chip.
        * @param PixHit := The Pixel Hit Object, for which the ToT is to be extracted.
        * @param RefPixHits := The Reference Pixel Hits in the Event. */

       bool IsIsolated(allpix::PixelHit* PixHit, std::vector<allpix::PixelHit*> RefPixHits) const;
};
#endif