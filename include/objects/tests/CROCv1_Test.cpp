#include <iostream>
#include <CROCv1.hpp>

LOGGER Messenger;

int main()
{
    CROCv1* ReadoutChip = new CROCv1(1000, 0, 70, 0, 0, {"Latched", "Double", "Double"}, 0, Messenger);
    ReadoutChip->PrintTornadoMap("Tornado.csv");
    return 0;
}