import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import moyal
import argparse

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})

parser = argparse.ArgumentParser(description = 'Exec Options')
parser.add_argument("--F", type = str)
args = parser.parse_args()

FILE_NAME = args.F

FILE = np.genfromtxt(FILE_NAME, delimiter = ", ")
X = []
Y = []

fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (7,3))

for i in range(0, len(FILE)):
    ax.plot([FILE[i][1], FILE[i][2]], [FILE[i][0], FILE[i][0]], color = 'red', lw = 2.0)
    ax.plot([FILE[i][3], FILE[i][3]], [-1, FILE[i][0]], color = 'blue', ls = '--')
    
#ax.axvline(1000, color = 'black', ls = '--', label = r"\texttt{THR} = 1000e")
ax.set_title(r"Nominal Reference Ladder @ \texttt{THR} = 1000 and \texttt{KRUM} = 70", fontsize = 15)
ax.set_xlabel(r"Charge [e]", fontsize = 15)
ax.set_ylabel(r"ToT Code", fontsize = 15)
ax.set_xlim([500, 30000])

Y_TICKS = np.arange(start = 0, stop = 16, step = 1)
Y_TICKS_STR = []
for i in Y_TICKS: Y_TICKS_STR.append(r"\texttt{" + str(i) + "}")
ax.set_yticks(ticks = Y_TICKS)
ax.set_yticklabels(labels = Y_TICKS_STR)
fig.savefig("RefLadder.png", dpi = 500, bbox_inches = "tight")

plt.show(block = True)
